using System;
using TMPro;
using UnityEngine;

namespace Code
{
    public class SimpleView : MonoBehaviour
    {
        public TextMeshPro bodyTextMesh;
        public Action okButtonAction;
        
        public void Open(string bodyText, Action action)
        {
            okButtonAction = action;
            bodyTextMesh.text = bodyText;
            gameObject.SetActive(true);
        }
        
        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
