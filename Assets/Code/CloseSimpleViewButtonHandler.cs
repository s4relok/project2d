using UnityEngine;

namespace Code
{
    public class CloseSimpleViewButtonHandler : SimpleButtonHandler
    {
        public SimpleView simpleView;
        
        public override void HandleClick()
        {
            simpleView.Close();
        }
    }
}
