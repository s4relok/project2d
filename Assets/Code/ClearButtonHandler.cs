using TMPro;

namespace Code
{
    public class ClearButtonHandler : SimpleButtonHandler
    {
        public TextMeshPro textMesh;
        
        public override void HandleClick()
        {
            textMesh.text = "";
        }
    }
}
