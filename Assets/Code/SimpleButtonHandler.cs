using UnityEngine;

namespace Code
{
    public abstract class SimpleButtonHandler : MonoBehaviour
    {
        [HideInInspector]
        public tk2dUIItem uiItem;

        private void OnEnable()
        {
            if (uiItem == null) uiItem = GetComponent<tk2dUIItem>();
            uiItem.OnClick += HandleClick;
        }
        
        private void OnDisable()
        {
            uiItem.OnClick -= HandleClick;        
        }

        public abstract void HandleClick();
    }
}
