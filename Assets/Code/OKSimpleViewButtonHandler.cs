namespace Code
{
    public class OKSimpleViewButtonHandler : SimpleButtonHandler
    {
        public SimpleView simpleView;
        
        public override void HandleClick()
        {
            simpleView.Close();

            if (simpleView.okButtonAction != null)
            {
                simpleView.okButtonAction();
            }
        }
    }
}
