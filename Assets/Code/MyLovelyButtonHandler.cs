using TMPro;

namespace Code
{
    public class MyLovelyButtonHandler : SimpleButtonHandler
    {
        public TextMeshPro textMesh;
        
        public override void HandleClick()
        {
            textMesh.text += "ВЫ НАЖАЛИ НА НЕЁ" + "\n";
        }
    }
}
