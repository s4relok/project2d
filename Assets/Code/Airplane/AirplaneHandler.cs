using UnityEngine;

namespace Code.Airplane
{
    public class AirplaneHandler : MonoBehaviour
    {
        public float speed;
        private GameObject target;
        
        
        private void Start()
        {
            target = GameObject.Find("AirplanesBlock/AirplaneIcon");
        }

        private void Update()
        {
            var directionToTarget = target.transform.position - transform.position;
            var distanceToTarget = directionToTarget.magnitude;
            if (distanceToTarget < 10)
            {
                Destroy(gameObject);
                return;
            }
            transform.position += directionToTarget * (speed * Time.deltaTime);
        }
    }
}
