using UnityEngine;

namespace Code
{
    public class AirPlaneButtonHandler : SimpleButtonHandler
    {
        public GameObject otherButton;
        
        public override void HandleClick()
        {
            otherButton.SetActive(!otherButton.activeSelf);
        }
    }
}
