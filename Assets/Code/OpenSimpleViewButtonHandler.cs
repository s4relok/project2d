using UnityEngine;

namespace Code
{
    public class OpenSimpleViewButtonHandler : SimpleButtonHandler
    {
        public SimpleView simpleView;
        public GameObject airplanePrefab;
        
        public override void HandleClick()
        {
            simpleView.Open("Вы хотите добавить самолет?", AddAirplane);
        }

        private void AddAirplane()
        {
            ModelScript modelScript = FindObjectOfType<ModelScript>();
            modelScript.AddAirplane();

            AnimateAirplane();
        }

        private void AnimateAirplane()
        {
            var airplane = Instantiate(airplanePrefab);
        }
    }
}
