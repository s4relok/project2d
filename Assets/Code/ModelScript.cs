﻿using Code;
using TMPro;
using UnityEngine;

public class ModelScript : MonoBehaviour
{

    private int airplanes;

    public int Airplanes
    {
        get => airplanes;
        set
        {
            airplanes = value;
            UpdateAirplanesLabel();
        }
    }

    public TextMeshPro airplanesLabel;
    public SimpleView simpleView;

    private void Start()
    {
        LoadGame();
    }


    public void AddAirplane()
    {
        Airplanes += 1;
        SaveGame();

        if (Airplanes >= 2000)
        {
            simpleView.Open("Вы победили!", null);
        }
    }

    private void LoadGame()
    {
        Airplanes = PlayerPrefs.GetInt("airplanes");
    }

    private void SaveGame()
    {
        PlayerPrefs.SetInt("airplanes", Airplanes);
    }

    private void UpdateAirplanesLabel()
    {
        airplanesLabel.text = Airplanes.ToString();
    }
}
