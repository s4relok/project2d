﻿using UnityEditor;

public class PreprocessTexturePixelPerMeterValue : AssetPostprocessor
{
    void OnPreprocessTexture ()
    {
        TextureImporter textureImporter  = (TextureImporter) assetImporter;
        var isHeroesAsset = textureImporter.assetPath.Contains("HeroesAssets");
        if(!isHeroesAsset) textureImporter.spritePixelsPerUnit = 1;
    }
}
